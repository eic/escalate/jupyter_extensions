import {
    Widget
} from '@phosphor/widgets';

import '../style/index.css';


/**
 * The default mime type for the extension.
 */
const MIME_TYPE = 'application/x-root';


/**
 * The class name added to the extension.
 */
const CLASS_NAME = 'jp-OutputWidgetROOT';

let root_is_loaded=false;

var loadJS = function(url, implementationCode, location){
    //url is URL of external file, implementationCode is the code
    //to be called from the file, location is the location to
    //insert the <script> element

    var scriptTag = document.createElement('script');
    scriptTag.src = url;

    // Multiple possible targets



    scriptTag.onload = implementationCode;
    scriptTag.onreadystatechange = implementationCode;

    location.appendChild(scriptTag);

};

var yourCodeToBeCalled = function(){
    console.log('hahaha');
    root_is_loaded = true;
//your code goes here
};


/**
 * A widget for rendering ROOT.
 */
export
class OutputWidget extends Widget {

    /**
     * Construct a new output widget.
     */
    constructor(options) {
        super();

        console.log('OutputWidget.constructor started');


        this.resolver = options.resolver;
        console.log('options');
        console.log(options);
        //getDownloadUrl: (url: string) => {
        //               expect(called0).to.equal(true);
        //               called1 = true;
        //               expect(url).to.equal('./foo%2520');
        //               return Promise.resolve(url);
        //             }
        console.log('this.resolver');
        this.resolver.getDownloadUrl('output.root').then(function(data) {
            console.log('all is complted', data);
        });
    }

    /**
     * Render ROOT into this widget's node.
     */
    renderModel(model) {
        console.log('there');
        var iframe;
        iframe = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = '100%';


        //iframe.src = '/rjs/?file='+this.resolver._session._path;
        this.resolver.getDownloadUrl(this.resolver.path).then(downloadUrl => {
            let staticPageUrl = this.resolver._contents.serverSettings.baseUrl + 'static/index.htm';
            //staticPageUrl = 'https://root.cern.ch/js/latest/';
            let rootFileUrl = this.resolver._contents.serverSettings.baseUrl + 'rjs/';

            console.log("downloadUrl:");
            console.log(downloadUrl);
            console.log("staticPageUrl:")
            console.log(staticPageUrl)
            console.log("iframe.src:")
            let iframe_link = staticPageUrl + '?file=/rjs/'+this.resolver.path;
            console.log(iframe_link);
            iframe.src = iframe_link;
        });

        console.log('JSroot link:');
        console.log(iframe.src);
        //iframe.style.display = 'none';
        console.log(this.node);
        console.log(model.metadata);
        this.node.appendChild(iframe);
    }
}



/**
 * A widget for rendering ROOT.
 */
export
class EveOutputWidget extends Widget {
    constructor(options) {
        super();
        console.log('eve');
    }

    renderModel(model) {
        console.log('render eve');
        var iframe;
        iframe = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.src = 'http://localhost:8080/?file=http://localhost:8008/open_charm_sm.root';

    }
}

/**
 * A mime renderer factory for ROOT data.
 */
export
const rendererFactory = {
    safe: true,
    mimeTypes: [MIME_TYPE],
    createRenderer: options => new OutputWidget(options)
};


const extension = {
    id: 'root-render:plugin',
    rendererFactory,
    rank: 0,
    dataType: 'string',
    fileTypes: [
        {
            name: 'root',
            fileFormat: 'base64',
            mimeTypes: [MIME_TYPE],
            extensions: ['.root']
        }
    ],
    documentWidgetFactoryOptions: {
        name: 'EicRender',
        modelName: 'base64',
        primaryFileType: 'root',
        fileTypes: ['root'],
        defaultFor: ['root']
    }
};

export default extension;
