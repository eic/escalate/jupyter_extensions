# Configuration file for jupyter-notebook.

#------------------------------------------------------------------------------
# NotebookApp(JupyterApp) configuration
#------------------------------------------------------------------------------

## Set the Access-Control-Allow-Origin header
#  
#  Use '*' to allow any origin to access your server.
#  
#  Takes precedence over allow_origin_pat.
c.NotebookApp.allow_origin = '*'

c.Spawner.args = ["--NotebookApp.allow_origin=*"]
c.JupyterHub.tornado_settings = {
    'headers': {
        'Access-Control-Allow-Origin': '*',
    },
}

print("HAHAHAHAHAHAHAHAHHSDHJHSDLJHSDGLKJH SFDGLKJHSD LK FGHD")

## Extra paths to search for serving static files.
#  
#  This allows adding javascript/css to be available from the notebook server
#  machine, or overriding individual files in the IPython


from os import path
this_script_dir = path.dirname(path.abspath(__file__))
static_dir = path.join(path.dirname(this_script_dir), 'static')

print("This __file__   = '{}'".format(__file__))
print("this_script_dir = '{}'".format(this_script_dir))
print("static_dir      = '{}'".format(static_dir))


c.NotebookApp.extra_static_paths = [static_dir]

# ROOT javascript is there:
import os
if 'ROOTSYS' in os.environ:
    c.NotebookApp.extra_static_paths.append(os.path.join(os.environ['ROOTSYS'], 'js'))


## Dict of Python modules to load as notebook server extensions.Entry values can
#  be used to enable and disable the loading ofthe extensions. The extensions
#  will be loaded in alphabetical order.
c.NotebookApp.nbserver_extensions = {
    'pyjano.notebook_extension': True
}
