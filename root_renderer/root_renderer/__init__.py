from IPython.display import display, JSON
import json

# Running `npm run build` will create static resources in the static
# directory of this Python package (and create that directory if necessary).

def _jupyter_nbextension_paths():
    return [{
        'section': 'notebook',
        'src': 'static',
        'dest': 'root_renderer',
        'require': 'root_renderer/extension'
    }]

# A display class that can be used within a notebook. 
#   from root_renderer import ROOT
#   ROOT(data)
    
class ROOT(JSON):
    """A display class for displaying ROOT visualizations in the Jupyter Notebook and IPython kernel.
    
    ROOT expects a JSON-able dict, not serialized JSON strings.

    Scalar types (None, number, string) are not allowed, only dict containers.
    """

    def _ipython_display_(self):
        bundle = {
            'application/x-root': self.data,
            'text/plain': '<root_renderer.ROOT object>'
        }
        metadata = {
            'application/x-root': self.metadata
        }
        display(bundle, metadata=metadata, raw=True) 
